(function() {
	'use strict';

	angular
	  .module('altairApp')
	  .service('consultasService', consultasService);

	consultasService.$inject = ['$q','$http'];

	function consultasService($q,$http) {
		var service=this;
		service.cargarConsultas=cargarConsultas;
		service.cargarAreas=cargarAreas;
		service.asignarArea=asignarArea;
		 var url = 'https://cjuridico-cuajahoo.c9users.io/back-end/webapis/api/api_cjuridico.php/';

        function cargarConsultas() {
            var defered = $q.defer();
            var urlRequest = url;
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
        function cargarAreas() {
            var defered = $q.defer();
            var urlRequest = url+"areas";
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
        function asignarArea(area) {
            var defered = $q.defer();
            var urlRequest = url+"asignarAreas/";
            $http.post(urlRequest,area).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
	}
})();