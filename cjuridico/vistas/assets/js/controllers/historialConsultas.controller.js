(function() {
    'use strict';

    app.controller('consultasController', consultasController);

    consultasController.$inject = ['NgTableParams', 'consultasService', 'localStorageService'];

    function consultasController(NgTableParams, consultasService, localStorageService) {
        var gestionConsultas = this;
        gestionConsultas.table_data=[];
        gestionConsultas.detalles={};
        gestionConsultas.meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio',
               'Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        gestionConsultas.usuario = localStorageService.get('user').id_persona;

        
        gestionConsultas.cargarConsultas = function() {
            consultasService.cargarConsultas(gestionConsultas.usuario).then(function(response) {
                angular.forEach(response.data, function(value) {
                    if(value.area_asignada){
                        value.estado="Asignada";
                        gestionConsultas.table_data.push(value);
                    }else{
                        value.estado="Sin asignar";
                      gestionConsultas.table_data.push(value);  
                    }
                });
                gestionConsultas.tablaConsultas = new NgTableParams({
                    page: 1,
                    count: 5
                }, {
                    total: gestionConsultas.table_data.length,
                    dataset: gestionConsultas.table_data
                });
            }).catch(function(error) {
                console.log(error);
            });
        };
        gestionConsultas.detallesConsulta=function(data){
            gestionConsultas.detalles=data;
            var fecha=new Date(gestionConsultas.detalles.fecha_consulta);
            gestionConsultas.detalles.fecha=String(fecha.getDate() +" de "+ gestionConsultas.meses[fecha.getMonth()]+" de "+fecha.getFullYear());
            $('#myModal').modal('show');
        };

    }
})();