(function() {
    'use strict';
    app.controller('ConsultorioController', ConsultorioController);
    //Se Inyectan las diirectivas a utilizar dentro del controlador
    ConsultorioController.$inject = ['$scope', 'JuridicoService', '$location'];

    function ConsultorioController($scope, JuridicoService, $location) {

        var control = this;
        control.personas = [];
        control.personasTemp = [];
        control.datos = {};
        //Guarda Los datos diligenciados en el formulario
        control.guardar = function() {
            var datosPersona = control.datos;
            JuridicoService.agregarPersona(datosPersona).then(function(response) {
                console.log(response.data.Radicado);
                swal("Consulta Exitosa!", "El radicado de tu consulta es: "+ response.data.Radicado , "success");
                $location.path("/Login");
            }).catch(function(error) {
                console.log(error);
            });
        };
        //Carga Los tipos De identificaciones estimulados en la base de datos 
        control.cargarTipoIdentificacion = function() {
            JuridicoService.tipo_iden().then(function(response) {
                control.tipos = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };
        //Carga Los estados civiles estimulados en la base de datos
        control.cargarEstadoCivil = function() {
            JuridicoService.estado_civil().then(function(response) {
                control.estados = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };
        control.cargarSexo = function() {
            JuridicoService.cargar_sexo().then(function(response) {
                control.sexos = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };
        control.cargarDepartamentos = function() {
            JuridicoService.cargar_departamentos().then(function(response) {
                control.departamentos = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };
        control.cargarMunicipios = function(id) {
            JuridicoService.cargar_municipios(id).then(function(response) {
                control.municipios = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        };


    }
})();