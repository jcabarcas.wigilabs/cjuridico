(function() {
    'use strict';
    app.controller('loginController', loginController);
    //Se Inyectan las diirectivas a utilizar dentro del controlador
    loginController.$inject = ['loginService', '$location', 'localStorageService'];

    function loginController(loginService, $location, localStorageService) {
        var control = this;
        control.datos = {};
        control.datos_login = {};
        /*Funcion login Se Encarga de enviar los datos a la
         base de datos para controlar el ingreso de los Usuarios*/
        control.login = function() {
            control.loading = true;
            var data = { usuario: control.datos.correo, pass: control.datos.pass };
            
            loginService.login(data).then(function(response) {
                if (response.data == null || response.data.length == 0) {
                    swal("Error!", "Usuario o contraseña incorrectos", "error");
                    control.loading = false;
                } else {
                    if (response.data[0]) {
                        if (response.data[0] == "<") {
                            swal("Error!", "Error al iniciar sesión intentelo nuevamente", "error");
                            control.loading = false;
                        } else {
                            control.datos_login = response.data[0];
                            localStorageService.set("user", control.datos_login);
                            $location.path("/Inicio");
                        }

                    } else {
                        swal("Error!", "Usuario o contraseña incorrectos", "error");
                        control.loading = false;
                    }
                }
            });
        };
    }
})();