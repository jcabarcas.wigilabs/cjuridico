(function() {
    'use strict';

    app.service('JuridicoService', JuridicoService);

    JuridicoService.$inject = ['$http', '$q'];

    function JuridicoService($http, $q) {
        var service = this;
        service.tipo_iden = tipo_iden;
        service.estado_civil = estado_civil;
        service.cargar_sexo = cargar_sexo;
        service.cargar_departamentos = cargar_departamentos;
        service.cargar_municipios = cargar_municipios;
        service.mostrarTabla = Mostrar;
        service.agregarPersona = Agregar;
        service.persona = {};
        var url = 'https://cjuridico-cuajahoo.c9users.io/back-end/webapis/api/api_cjuridico.php/';

        function Mostrar() {
            var defered = $q.defer();
            var urlRequest = url;
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }

        function Agregar(object) {
            var defered = $q.defer();
            var urlRequest = url;
            $http.post(urlRequest, object).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }

        function tipo_iden() {
            var defered = $q.defer();
            var urlRequest = url + "tipo_identificacion/";
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
        function estado_civil() {
            var defered = $q.defer();
            var urlRequest = url + "estado_civil/";
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
        function cargar_sexo() {
            var defered = $q.defer();
            var urlRequest = url + "cargar_sexo/";
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
        function cargar_departamentos() {
            var defered = $q.defer();
            var urlRequest = url + "cargar_departamentos/";
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
        function cargar_municipios(id) {
            var defered = $q.defer();
            var urlRequest = url + "cargar_municipios/"+id;
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
    }
})();