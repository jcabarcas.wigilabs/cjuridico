(function() {
	'use strict';

	app.service('loginService', loginService);

	loginService.$inject = ['$http','$q'];

	function loginService($http,$q) {
		var service=this;
		service.login=login;
        var url = 'https://cjuridico-cuajahoo.c9users.io/back-end/webapis/api/api_cjuridico.php/';

        function login(object) {
            var defered = $q.defer();
            var urlRequest = url+"personas/login";
            $http.post(urlRequest, object).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
        }
	}
})();