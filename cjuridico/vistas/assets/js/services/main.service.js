(function() {
	'use strict';

	app.service('mainService', mainService);

	mainService.$inject = ['$http','$q'];

	function mainService($http,$q) {
		var servicio=this;
		var url = 'https://cjuridico-cuajahoo.c9users.io/back-end/webapis/api/api_cjuridico.php/';
		servicio.cargarConsultas=cargarConsultas;
		servicio.cargarInformacionPersonal=cargarInformacionPersonal;

		function cargarConsultas(radicado){
			 var defered = $q.defer();
            var urlRequest = url+"verificarConsultas/"+radicado;
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
		}
		function cargarInformacionPersonal(id){
			 var defered = $q.defer();
            var urlRequest = url+"informacionPersonal/"+id;
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
		}
	}
})();