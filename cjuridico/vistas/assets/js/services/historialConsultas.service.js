(function() {
	'use strict';

	app.service('consultasService', consultasService);

	consultasService.$inject = ['$http','$q'];

	function consultasService($http,$q) {
		var servicioConsultas=this;
		var url = 'https://cjuridico-cuajahoo.c9users.io/back-end/webapis/api/api_cjuridico.php/';
		servicioConsultas.cargarConsultas=cargarConsultas;

		function cargarConsultas(user){
			 var defered = $q.defer();
            var urlRequest = url+"consultas/"+user;
            $http.get(urlRequest).then(function(response) {
                defered.resolve(response);
            }).catch(function(error) {
                defered.reject(error);
            });
            return defered.promise;
		}

	}
})();